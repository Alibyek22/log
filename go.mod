module code.gitea.io/log

go 1.12

require (
	github.com/mattn/go-isatty v0.0.7
	github.com/stretchr/testify v1.3.0
	golang.org/x/sys v0.0.0-20190429190828-d89cdac9e872
)
