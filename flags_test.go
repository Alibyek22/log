// Copyright 2019 The Gitea Authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package log

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

type testFlags struct {
	Flags Flags `json:"flags"`
}

func TestFlagsMarshalUnmarshalJSON(t *testing.T) {
	flagsBytes, err := json.Marshal(testFlags{
		Flags: LstdFlags,
	})
	assert.NoError(t, err)
	assert.Equal(t, string(makeTestFlagsBytes("stdflags")), string(flagsBytes))

	var testFlags testFlags
	err = json.Unmarshal(makeTestFlagsBytes(`XAXAXA`), &testFlags)
	assert.NoError(t, err)
	assert.Equal(t, Flags(0), testFlags.Flags)

	err = json.Unmarshal([]byte(fmt.Sprintf(`{"flags":%d}`, LstdFlags)), &testFlags)
	assert.NoError(t, err)
	assert.Equal(t, LstdFlags, testFlags.Flags)

	flagString := "date, shortfile, levelinitial"
	err = json.Unmarshal(makeTestFlagsBytes(flagString), &testFlags)
	assert.NoError(t, err)
	assert.Equal(t, FlagsFromString(flagString), testFlags.Flags)
	flagsBytes, err = json.Marshal(testFlags)
	assert.NoError(t, err)
	assert.Equal(t, string(makeTestFlagsBytes(flagString)), string(flagsBytes))
}

func makeTestFlagsBytes(flagsString string) []byte {
	return []byte(fmt.Sprintf(`{"flags":"%s"}`, flagsString))
}
